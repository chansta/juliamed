# juliaMED

## Brief Description

This Julia package contains several routines relating to the computation of Maximum Entropy Density (MED). The main aim is to generates the parameters in the MED given a sequence of moments. Checks will be conducted to ensure the existence of the MED solution. The conditions are mainly based on Tagliani (1994) and the computation of the MED parameters based on the sequence of moments follows Rockinger and Jondeau (2002). 

The module depends on an artifact which can be installed by [juliaMEDInitCoef](https://gitlab.com/chansta/juliamedinitcoef). Please install the artifact before installing this package.  

## What it can do so far

Users can provide a vector containing a sequence of moments and there exists routine that can 

1. Check if the provided sequence has a correponding MED solution.
2. Compute those parmaeters. 
3. In computing those parameters, the users can provide their own initial values. The function can also automatically calcualte the initial values based on a quadratic approximation of the parameters using the sequence of moments provided. Only k=4 is being implemented at this stage. The plan is include all k. 

The check is based on the conditions stated in Tagliani (1994). However the case when **k=6** is missing becasue I am unable to obtain the original paper i.e. Tagliani (1992) where the exact condition was stated. It would be greatly appreciated if anyone who has that paper can share with me. 

## Limitation 
1. Condition on the existence of MED solution when k=6 is missing. So please do not rely on the check if the sequence of moments is even i.e. odd moments are zeros and k=6. However, k=6 in the uneven case, i.e. at least one odd moment is not zero, works fine. 

2. Initial value generation is limited to k=4. This will be extended soon. 





