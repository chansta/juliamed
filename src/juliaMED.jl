"""
    module juliaMED

## Descrption:
    
    This module contains a collection of routines and functions relating to the estimation of MED. 

"""
module juliaMED

    using CSV
    using DataFrames; const df = DataFrames
    using QuadGK
    using Optim
    using LinearAlgebra
    using Pkg.Artifacts
    using juliaMEDInitCoef

    include("potential.jl")
    include("med.jl")
#################################################################################################################################
"""
    mu2MEDObj (mu::Array{Float64,1}

## Description:

    Object that store a sequence of moments which has a MED soltion. Initiation of an instance will only be possible if the sequence of moments satisfied the conditions as stated in Tagliani (1994). 

"""
    struct mu2MEDObj
	mu::Array{Float64,1}
	mu2MEDObj(mu) = checkExistence(mu) ? new(mu) : error("This sequence of moments does not have a MED solution.")
    end 

#################################################################################################################################

"""
    lambdaResult(muObj::muObj, l::Array{Float64}(k), optimResult::Any)

## Description: 
    Object containing the output of the estimated parameter vector of a MED based on a sequence of moments. 

"""
    struct lambdaResult
	muObj::mu2MEDObj
	l::Array{Float64,1}
	optimResult::Any
    end

#################################################################################################################################
"""
    getLambda(muObj::mu2MEDObj, l0::Array{Float64,1}=[0,-0.5,0,0] ; autoInitial::Bool=true, filename::String="default")

## Description:
    
    It computes the corresponding parameter vector in the MED based on the sequence of moments a defined in the mu2MEDObj. The vector is computed by minimizaing the MED potential as proposed in Rockinger and Jondeau (2002). 

## Inputs:
    
    muObj: mu2MEDObj. mu object that contains the sequence of moments that satisies the conidtions as stated in Tagliani (1994). 

    l0: Array{Float64,1}(k). Intial values for the minimization of the MED potential. This input will be ignored if autoInitial is true (see below). 

    autoInitial: Bool. If true, the initial values will be computed automatically based on a quadratic approximation (see getInitialValue). In this case, l0 will be ignored. 

## Outputs
    
    lambdaResult: An instance of the lambdaResult object.
"""
    function getLambda(muObj::mu2MEDObj, l0::Array{Float64,1}=[0,-0.5,0,0] ; autoInitial::Bool=true, filename::String="default")
	mu = muObj.mu
	l,optimResult = minQPotential(mu,l0,autoInitial=autoInitial, filename=filename)
	return lambdaResult(muObj,l,optimResult)
    end

export getLambda
export InitCoefPath
end

