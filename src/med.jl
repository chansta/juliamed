using QuadGK
using Optim
using CSV
using DataFrames; const df = DataFrames

#################################################################################################################################
"""
MEDExp(x::Flaot64,l::Array{Float64,1})

## Description
    Calculate the non-normalised value of MED. 

## Inputs:
    x: Float64. A point of the function. 
    l: Array{Float64,1}{k}. The sequence of ``\\lambda``. 

## Output
    MED: Float64. The non-normalised value of MED at x. 

"""
function MEDExp(x::Float64,l::Array{Float64,1})
    k = size(l,1)
    xset = [x^i for  i in 1:k]
    return exp(transpose(l)*xset)
end

#################################################################################################################################
"""
getMEDQ(l::Array{Float64,1})

## Description:
    Evaluate the integral of MED i.e. the normalising constant. 

## Inputs:

    l: Array{Float64,1}(k). The sequence of lambda. 

    lowerbound: Float64. The lowerbound of the integral. -Inf for Hamburger case and 0 for the Stieltjes case.  

## Output:
    MED: Float84. The normalising constant - if exists. 
"""
function getMEDQ(l::Array{Float64,1}; lowerbound::Float64=-Inf)
    temp(x) = MEDExp(x,l)
    return quadgk(temp, lowerbound, Inf)
end

#################################################################################################################################
"""
MED(x,l,Q)

## Description:
    Evaluate the MED at a given point. 

## Inputs:
    
    x: Float64. Point of the function. 

    l: Array{Float64,1}(k). The sequence of ``\\lambda``. 

    Q: The normalising constant. Most likely come from getMEDQ. 

## Output:
    MED: Float64. Value of the MED at point x.  
"""
function MED(x::Float64,l::Array{Float64,1},Q::Float64)
    return MEDExp(x,l)/Q
end

#################################################################################################################################
"""
getMu(l::Array{Float64,1})

## Description:
    Evaluate the first k raw moments of MED given a sequence of ``\\lambda``. 

## Inputs:
    
    l: Array{Float64,1}(k). The sequence of ``\\lambda``. 

    lowerbound: Float64. The lowerbound of the integral. -Inf for Hamburger case and 0 for the Stieltjes case.  

## Output:
    mu: Array{Float64,1}(k). The corresponding raw moments. 
"""
function getMu(l::Array{Float64,1}; lowerbound::Float64=-Inf)
    k = size(l,1)
    Q = getMEDQ(l)[1]
    tempfunc(x) = MED(x,l,Q)
    muset = [quadgk(x-> (x^i)*tempfunc(x), lowerbound, Inf)[1] for i in 1:k]
    return muset
end

#################################################################################################################################
"""
getHankel(mu::Array{Float64,1})

## Description: 
  
Construct the Hankel matrix based on the sequecne of moment mu. Only the even case i.e. even number of moments are considered. 

## Inputs:

    mu: Array{Float64,1}(k). The sequence of moments. 

## Outputs:

    Union{Aaray{Float64,1}, 0}: Either the Hankel matrix or 0 if the number of moment is uneven. 

"""
function getHankel(mu::Array{Float64,1})
    k = size(mu,1)
    if iseven(k)
        mu1 = vcat(1,mu)
        hankel = mu1[1:Int(k/2)+1]
        for i in 2:Int(k/2)+1
            hankel = hcat(hankel, mu1[i:i+Int(k/2)])
        end
        return hankel
    else
        println("The case when k is not even has not yet been implemented.")
        return 0
    end
end

#################################################################################################################################
"""
checkPos(h::Array{Float64,2})

## Description:

Check if a matrix is positive semi-definite. 

## Inputs:

    h: Array{Float64,2}(k,k). Matrix to be checked. 

## Output:

    Bool: true if the matrix is positive semi-definite. 

"""
function checkPos(h::Array{Float64,2})
    e = eigvals(h)
    if any(e.<0) 
        return false
    else
        return true
    end
end

#################################################################################################################################
"""
checkEvenCase(mu::Array{Float64,1})

## Description:

Check if the sequence of moments is an even case i.e. all odd moments are 0.

## Input:

    mu: Array{Float64, 1}(k): The sequence of moments. 

## Output:

    Bool: ture if it is an even case. 
"""
function checkEvenCase(mu::Array{Float64,1})
    k = size(mu,1)
    select = filter(isodd, 1:k)
    return all(mu[select].==0)
end

#################################################################################################################################
"""
checkUpperBound(mu::Array{Float64,1})

## Descriptions:

    Check the sequence moments satisfied the upper bound conditions as stated in Tagliani (1994). The case when ``k=6`` is not complete as it is unclear the exact forms of the weber functions. 

## Input:

    mu: Array{Float64,1}(k). The sequence of moments. 

## Output:

    Bool: true if the upper bound condition is satisfied. 

"""
function checkUpperBound(mu::Array{Float64,1})
    k = size(mu,1)
    if k == 4
        return mu[4] <= 3*mu[2]^2
    elseif k==6
        println("The case there k=6 is not complete.")
        return (mu[4] <= 3*mu[2]^2) #needs to add the condition of the weber function. 
    else
        return true
    end
end

#################################################################################################################################
"""
checkExistence(mu::Array{Float64, 1})

## Descriptions:

Check the sequence moments satisfied the upper bound conditions as stated in Tagliani (1994). The case when ``k=6`` is not complete as it is unclear the exact forms of the weber functions. 

## Input:

    mu: Array{Float64,1}(k). The sequence of moments. 

## Output:

    Bool: true if the sequence of moments have a MED solution. 

"""
function checkExistence(mu::Array{Float64, 1})
    h = getHankel(mu)
    if checkPos(h) 
        if checkEvenCase(mu)
            return checkUpperBound(mu)
        else
            return true
        end
    else
        return false
    end
end

#################################################################################################################################
