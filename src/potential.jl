using QuadGK
using Optim
using CSV
using DataFrames; const df=DataFrames
using juliaMEDInitCoef


#################################################################################################################################
"""
getQExp(x::Float64, mu::Array{Float64,1}, l::Array{Float64,1})

## Description: 
    Compute the exponent of the MED potential. The function also allows a reparametrization which guarantee the existence of integral.

## Inputs: 

    x: Float64. A point of the function. 

    mu: Array{Float64,1}(k). The moment sequence. 

    l: Array{Float64,1}(k). The lambda vector. 

    reparam: Bool. If true, l[k] will be set to -l[k]^2 (ensure the coefficient of the highest order is negative.)

## Output:

    The exponent of the MED. 

"""
function getQExp(x::Float64, mu::Array{Float64,1}, l::Array{Float64,1}; reparam::Bool=false)
    k = size(mu, 1)
    powerset = [x^i for i in 1:k]
    templ = copy(l)
    if reparam == true
        templ[k] = -templ[k]^2
    end
    g = transpose(templ)*(powerset-mu)
    return exp(g)
end

#################################################################################################################################
"""
getQDeriveExp(x::Float64, mu::Array{Float64,1},l::Array{Float64,1}; order::Int64=1, reparam::Bool=false)

## Description: 

   Compute the exponent of the specified partial derivative of the MED potential.  The function also allows a reparametrization which guarantee the existence of integral.

## Inputs: 

    x: Float64. A point of the function. 

    mu: Array{Float64,1}(k). The moment sequence. 

    l: Array{Float64,1}(k). The ``\\lambda`` vector. 

    order: Int64. The order^th partial derivative. Default=1. 

    reparam: Bool. If true, l[k] will be set to -l[k]^2 (ensure the coefficient of the highest order is negative.)

## Output:

    The exponent of the order^th partial derivative of the MED potential. 
"""
function getQDeriveExp(x::Float64, mu::Array{Float64,1},l::Array{Float64,1}; order::Int64=1, reparam::Bool=false)
    k = size(mu,1)
    getQDE = (x^order-mu[order])*getQExp(x,mu,l,reparam=reparam)
    if reparam == true && order == k
        getQDE = -2*l[k]*getQDE
    end 
    return getQDE
end

#################################################################################################################################
"""
getQ(mu::Array{Float64,1}, l::Array{Float64,1}; reparam::Bool=false)

## Description: 
    Compute the MED potential.  The function also allows a reparametrization which guarantee the existence of integral.

## Inputs: 
   
    mu: Array{Float64,1}(k). The moment sequence. 
   
    l: Array{Float64,1}(k). The lambda vector. 

    reparam: Bool. If true, l[k] will be set to -l[k]^2 (ensure the coefficient of the highest order is negative.)

    lowerbound: Float64. The lowerbound of the integral. -Inf for Hamburger case and 0 for the Stieltjes case.  

## Output:

    MED potential. 
"""
function getQ(mu::Array{Float64,1}, l::Array{Float64,1}; reparam::Bool=false, lowerbound::Float64=-Inf)
    Q(x) = getQExp(x, mu, l, reparam=reparam)
    return quadgk(Q, lowerbound, Inf)
end

#################################################################################################################################
"""
getQDerive(mu::Array{Float64,1},l::Array{Float64,1};order::Int64=1, reparam::Bool=false)

## Description: 

    Compute the gradient vector of the MED potential.  The function also allows a reparametrization which guarantee the existence of integral.

## Inputs: 

    mu: Array{Float64,1}(k). The moment sequence. 

    l: Array{Float64,1}(k). The lambda vector. 

    reparam: Bool. If true, l[k] will be set to -l[k]^2 (ensure the coefficient of the highest order is negative.)

    lowerbound: Float64. The lowerbound of the integral. -Inf for Hamburger case and 0 for the Stieltjes case.  

## Output:

    MED potential: Array{Flaot64,1}(k) 
"""
function getQDerive(mu::Array{Float64,1},l::Array{Float64,1};order::Int64=1, reparam::Bool=false, lowerbound::Float64=-Inf)
    Q(x) = getQDeriveExp(x,mu,l,order=order, reparam=reparam)
    return quadgk(Q, lowerbound, Inf)
end

#################################################################################################################################
"""
getQReduce(g::Float64, mu::Array{Float64,1}, l::Array{Float64,1}; reparam::Bool=false)

## Description: 
    A function to implement the steepest descent algorithm. 

## Inputs:

    g: Float64. The directional scalar. 

    mu: Array{Float64,1}(k). The moment sequence. 

    l: Array{Float64,1}(k). The lambda vector. 

    reparam: Bool. If true, l[k] will be set to -l[k]^2 (ensure the coefficient of the highest order is negative.)

## Output:

    getQ: Float64. The potential value after l has been shifted to its gradient value by a factor of g. 
"""
function getQReduce(g::Float64, mu::Array{Float64,1}, l::Array{Float64,1}; reparam::Bool=false)
    k = size(l, 1)
    gradient = [getQDerive(mu,l,order=r, reparam=reparam)[1] for r in 1:k]
    l0 = l - g*gradient
    return getQ(mu,l0, reparam=reparam)[1]
end

#################################################################################################################################
"""
getCovExp(x::Float64, mu::Array{Float64,1}, l::Array{Float64,1}, p::Int64, q::Int64; reparam::Bool=false)

Description: Compute the exponent of the Hessian matrix of the MED potential.  The function also allows a reparametrization which guarantee the existence of integral.

Inputs: 
    
    x: Float64. A point of the potential. 

    mu: Array{Float64,1}(k). The moment sequence. 
    
    l: Array{Float64,1}(k). The lambda vector. 

    p: Int in [1,k]. The i^th element of the matrix. 

    q: Int in [1,k]. The j^th element of the matrix.
    
    reparam: Bool. If true, l[k] will be set to -l[k]^2 (ensure the coefficient of the highest order is negative.)

Output:

    The (i,j) element in the exponent of the Hessian of the MED potential: Flaot64
"""
function getCovExp(x::Float64, mu::Array{Float64,1}, l::Array{Float64,1}, p::Int64, q::Int64; reparam::Bool=false)
    k = size(l,1)
    xset = [x^i for i in 1:k]
    dev = (x^p-mu[p])*(x^q-mu[q])*getQExp(x, mu, l, reparam=reparam)
    if reparam == true
        if p == k && q==k 
            dev = -2*(x^k-mu[k])*getQExp(x, mu, l, reparam=reparam) + dev*4*l[k]^2
        elseif p==k || q==k
            dev = -2*l[k]*dev
        end
    end
    return dev 
end

#################################################################################################################################
"""
getCov(mu::Array{Float64,1}, l::Array{Float64,1}, p::Int64, q::Int64; reparam::Bool=false)

## Description: Compute the (i,j) element of Hessian matrix of the MED potential.  The function also allows a reparametrization which guarantee the existence of integral.

## Inputs: 

    mu: Array{Float64,1}(k). The moment sequence. 
    
    l: Array{Float64,1}(k). The lambda vector. 

    p: Int in [1,k]. The i^th element of the matrix. 

    q: Int in [1,k]. The j^th element of the matrix.
    
    reparam: Bool. If true, l[k] will be set to -l[k]^2 (ensure the coefficient of the highest order is negative.)

    lowerbound: Float64. The lowerbound of the integral. -Inf for Hamburger case and 0 for the Stieltjes case.  

## Output:

    The (i,j) element in the Hessian of the MED potential: Flaot64
"""
function getCov(mu::Array{Float64,1}, l::Array{Float64,1}, p::Int64, q::Int64; reparam::Bool=false, lowerbound::Float64=-Inf)
    obj(x) = getCovExp(x, mu, l, p, q, reparam=reparam)
    return quadgk(obj, lowerbound, Inf)
end

#################################################################################################################################
"""
getGradientVector(mu::Array{Float64.1},l::Array{Float64.1}; reparam::Bool=false)

## Description: Get the gradient vector of the potential. 

## Inputs:

    mu: Array{Float64,1}(k). The moment sequence. 
    
    l: Array{Float64,1}(k). The lambda vector. 
    
    reparam: Bool. If true, l[k] will be set to -l[k]^2 (ensure the coefficient of the highest order is negative.)

## Output:
    
    gradient: Array{Float64,1}(k)

"""
function getGradientVector(mu::Array{Float64,1},l::Array{Float64,1}; reparam::Bool=false)
    k = size(mu,1)
    return [getQDerive(mu,l,order=i, reparam=reparam)[1] for i in 1:k]
end

#################################################################################################################################
"""
getHessian(mu::Array{Float64.1},l::Array{Float64.1}; reparam::Bool=false)

## Description: 

    Get the Hessian matrix of the potential. 

## Inputs:

    mu: Array{Float64,1}(k). The moment sequence. 
    
    l: Array{Float64,1}(k). The lambda vector. 
    
    reparam: Bool. If true, l[k] will be set to -l[k]^2 (ensure the coefficient of the highest order is negative.)

## Output:
    
    hessian: Array{Float64,2}(k,k)

"""
function getHessian(mu::Array{Float64,1},l::Array{Float64,1}; reparam::Bool=false)
    k = size(mu,1)
    return [getCov(mu,l,p,q, reparam=reparam)[1] for p in 1:k, q in 1:k]
end

#################################################################################################################################
"""
getInitialValue(mu::Array{Float64,1}; filename::String="default")

## Descriptions:
	
	Get the initial values for lambda_0 given the sequence of moment mu. The initla value is calculated based on the quadratic approximation between lambda and mu over a range of lambda. The coefficients are stored in the file located in the directory "resources". Though users supplied coefficients are also allowed. 

	**NB:** Only k=4 is being implemented for now. 

## Inputs:

    mu: Array{Float64,1}{4}. The sequence of moments. 

    filename: String. The file name of the coefficients. 

## Outputs:

    l0: Array{Float64,1}{4}. Approximated initial value of lambda. 

"""
function getInitialValue(mu::Array{Float64,1}; filename::String="default")
    if size(mu,1) != 4
	println("Only k=4 is being implmented at this stage") 
	return 0
    else
	if filename == "default"
	    #coeff = [1.940166912762012e-14 -0.23644109934331403 1.5518842992352094e-14 -2.1557228500664576; 0.8105473355970613 -3.689569808527915e-14 0.7701419624215067 -4.8697658861125703e-14; -7.015535477853871e-14 0.6794247195843062 -5.703094517906654e-14 3.096978805962884; -0.0788569120800008 5.5541619006370564e-15 -0.05826667911089488 5.36932321792017e-15; 3.990951246237752e-14 -0.19045223408211154 2.999924392016115e-14 -0.31596075054757106; 3.812846306044948e-14 -0.42176243975528477 1.6745638079310177e-14 -1.2670499269686568; -5.3085506368337476e-14 0.23430468574377422 -3.640726500657443e-14 -0.1681206134264014; 1.48598970991674e-15 -0.006393823136408077 5.592755262812804e-16 0.04905523194404505; -3.5295869529531596e-17 0.00019607494935255967 -1.3793547734232056e-17 -0.0015121271025221675]
	    artifact_toml = joinpath(dirname(pathof(juliaMEDInitCoef)), "Artifacts.toml")
	    coeff_hash = artifact_hash("InitCoef", artifact_toml)
	    filepath = artifact_path(coeff_hash)
	    coeff = Matrix(CSV.read(joinpath(filepath, "coefficients-K02.csv"), header=false))
	    println(filepath)
	else
	    coeff = Matrix(CSV.read(filename, header=false))
	end
	l0 = coeff[1,:] + transpose(coeff[2:end,:])*vcat(mu, mu.^2)
	return l0
    end
end

#################################################################################################################################
"""
minQPotential(mu::Array{Float64,1}, l0::Array{Float64,1}=[0,-0.5,0,-0.01]; autoInitial::Bool=true)

## Descriptions:
    Compute lambda for the MED given a sequence of moment mu by minimising the potential of MED. This method follows from Rockinger and Jondeau (2002). 

## Inputs:
    
    mu: Array{Float,4}{k}. Sequence of moments. 
    
    l0: Initial value. Ignored if autoInitial is true. 
    
    autoInitial: Bool. If true, initial values will be generated automatically using getInitialValue (see InitialValue). 

    filename: String. The file name of the coefficients. Default is "default"

## Outputs
    
    l: Array{Float64,1}(k). The lambda vector. 
    optimresult: MultivariateOptimizationResults. The result object from minimising the ptential of MED. 


"""
function minQPotential(mu::Array{Float64,1}, l0::Array{Float64,1}=[0,-0.5,0,-0.01]; autoInitial::Bool=true,  filename::String="default")
    if autoInitial == false
	x0 = l0
    else
	x0 = getInitialValue(mu, filename=filename)
    end
    k = size(l0,1)
    obj(x) = getQ(mu,x, reparam=true)[1]
    optimresult = optimize(obj,x0)
    l = optimresult.minimizer
    l[k] = -l[k]^2
    return l, optimresult
end

#################################################################################################################################

